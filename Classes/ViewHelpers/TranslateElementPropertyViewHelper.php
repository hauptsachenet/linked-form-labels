<?php

namespace Hn\LinkedFormLabels\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\GenericFormElement;
use TYPO3\CMS\Form\Domain\Model\Renderable\RootRenderableInterface;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class TranslateElementPropertyViewHelper extends AbstractViewHelper
{
    protected $escapeChildren = false;
    protected $escapeOutput = false;

    public function initializeArguments(): void
    {
        $this->registerArgument('element', RootRenderableInterface::class, 'Form Element to translate', true);
        $this->registerArgument('property', 'mixed', 'Property to translate', false);
        $this->registerArgument('renderingOptionProperty', 'mixed', 'Property to translate', false);
    }

    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $result = \TYPO3\CMS\Form\ViewHelpers\TranslateElementPropertyViewHelper::renderStatic($arguments, $renderChildrenClosure, $renderingContext);

        if (!is_string($result)) {
            return $result;
        } else {
            // i need to apply special chars afterwards
            $result = htmlspecialchars($result);
        }

        /** @var RootRenderableInterface $element */
        $element = $arguments['element'];
        $elementType = $element->getType();
        if (!in_array($elementType, ['Checkbox'])) {
            return $result;
        }

        if (!$arguments['element'] instanceof GenericFormElement) {
            return $result;
        }

        $properties = $arguments['element']->getProperties();
        if (!isset($properties['linkPageUid']) || empty($properties['linkPageUid'])) {
            return $result;
        }

        $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        $link = $contentObject->typoLink($properties['linkText'], [
            'parameter' => $properties['linkPageUid'],
            'target' => '_blank',
        ]);

        return sprintf($result, $link);
    }

}