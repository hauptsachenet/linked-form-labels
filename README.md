## installation

- `composer require hn/linked-form-labels`
- enable the extension in extension manager.

Nothing more to be done. This extension does have a little typoscript but that is though `ext_typoscript_setup.txt`.

## what it does

Adds `linkText` and `linkPageUid` to every normal form type. Every label will than be passed though `sprintf` to where `%s` is replaced with that link.

## limitations

- kind of ugly
- only 1 link per label
- editor has to use markers (%s)