<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'linked_form_labels',
    'description' => '',
    'category' => '',
    'author' => '',
    'author_email' => '',
    'author_company' => '',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '',
    'constraints' => [
        'depends' => [
            'form' => '*'
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];